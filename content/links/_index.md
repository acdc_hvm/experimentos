## Sitios amigos


#### Taller La Física al Alcance de Todos

* [**ACDC (Asociación civil de ex-alumnos de Daniel Cordoba)**](http://www.acdc.org.ar/)

* [**Blog del Nivel 3**](https://nivel3-2022.blogspot.com/)

* [**Blog del Nivel 4**](https://nivel4-2023.blogspot.com)

* [**Blog de Salta la Física**](https://saltalafisica.blogspot.com/)

* [**Sala Relatos - Homenaje Virtual a Daniel Córdoba**](https://app.gather.town/app/Fl0b9zzNAo3qAu75/relatos)

* [**Facebook**](https://www.facebook.com/saltalafisica)

* [**Instagram**](https://www.instagram.com/username)

* [**YouTube**](https://www.youtube.com/channel/UC0UP8MoSkFK9o4AVZDEGSmA)

* [**Correo**](saltalafisica@gmail.com)

