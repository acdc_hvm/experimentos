---
title: "Velocidad del sonido con Smartphone"
subtitle: 📢🏃📱
math: true
author: Martín Aramayo, Alberto Villagran
image: "/page/medir-distancias-con-trigonometria/regla-mano-izquierda-portrait.webp"
tags: ["Sonido", "Cinemática", "MRU", "Smartphone", "Phyphox"]
date: 2022-10-19
image: false
--- 

Medición de velocidad del sonido usando dos smartphones con Phyphox

<!--more-->

- - -
* **Dificultad:** ★★☆☆☆

* **Materiales:** 
    * 2 Smartphones (con la app [Phyphox](https://phyphox.org/))
    * Cinta métrica

* **Duración de preparación:** 40 min

* **Temas:** Sonido, cinemática.
- - -

La gente de Phyphox armo estos experimentos, los hacemos disponibles traducidos. Dejo link al [original](https://phyphox.org/experiment/speed-of-sound/) con los [ejercicios](https://phyphox.org/material/worksheet_speed-of-sound.docx).
 
![](/img/experimento-sonido-phyphox.webp)


### Pasos

1.  Instalar Phyphox en los dos smarthpones.

2.  Elegir **Cronómetro acústico (Acoustic Stopwatch).**

![](/img/velocidad-sonido-phyphox1.jpg)

3.  Configurar el **Umbral (Threshold)** a 0.8 para reducir el ruido de fondo.
4.  Configurar el **Delay mínimo (Minimum Delay)** a 0.1.

![](/img/velocidad-sonido-phyphox2.jpg)

5.  Para hacer el experimento, colocar los teléfonos separados uno del otro y medir esa distancia. Recomendamos que sea de 1 m (\\(d\\) = 1 m).

6.  Aplaude una vez. El teléfono más cercano empezará antes. El segundo teléfono (el más lejano) empezará después. Por lo tanto, habrá un 
    \\(\mathrm{\Delta}t\\).

7.  Tan pronto como tu amigo escuche el aplauso, tiene que aplaudir para detener su teléfono. El tiempo que va a medir será \\(t_{1}\\). El aplauso de tu amigo va a detener, luego, tu temporizador en tu teléfono. Este medirá \\(t_{2}\\).

8.  En cada intento, hay 2 aplausos (cada uno aplaude una vez), entonces hay dos tiempos:

$$t_{2} - \ t_{1} = \mathrm{\Delta}t.$$

9.  Intenta el experimento 4 veces, recopilando 4 intentos.

10. **NOTA:** El Sonido ha viajado la distancia entre los teléfonos dos veces. Esto es 1 m x 2. 
    Por lo tanto, tenés que, o duplicar la distancia o dividir el intervalo de tiempo por dos 
    (\\(\Delta t \over 2\\)).

11. Calcular la velocidad del sonido usando:

$$\mathrm{\Delta}t = \frac{d}{v}$$

12. Calcula el promedio de los 4 intentos y verifica la precisión y exactitud del resultado.

13. Completa la tabla.

14. Recuerda aplaudir, tan pronto **escuches** el aplauso de tu amigo.

15. Mira el video del experimento (en inglés): https://youtu.be/uoUm34CnHdE

**Intento**   | **Tiempo (s)**   | $${\mathrm{\Delta}t} (s)$$        | **Distancia (m)** |  **Velocidad del sonido (m/s)**
--------------| -----------------| ----------------------------------|-------------------|---------------------------------------
1             | $$t_{1} =$$      |                                   |                   |                              
1             | $$t_{2} =$$      |                                   |                   |                              
--------------| -----------------| ----------------------------------|-------------------|---------------------------------------
2             | $$t_{1} =$$      |                                   |                   |                              
2             | $$t_{2} =$$      |                                   |                   |                      
--------------| -----------------| ----------------------------------|-------------------|---------------------------------------
3             | $$t_{1} =$$      |                                   |                   |                              
3             | $$t_{2} =$$      |                                   |                   |                      
--------------| -----------------| ----------------------------------|-------------------|---------------------------------------
4             | $$t_{1} =$$      |                                   |                   |                              
4             | $$t_{2} =$$      |                                   |                   |                              
--------------| -----------------| ----------------------------------|-------------------|---------------------------------------
Exactitud     |                  |                                   |                   |                          
Precisión     |                  |                                   |                   |                          

$$
\quad
$$

$$
\quad
$$


Velocidad promedio del sonido = ________________ m/s.

### Pregunta

¿Qué pasa si cambiamos la distancia entre los teléfonos?
Intentá con diferentes distancias e investiga el resultado. Podés usar la tabla otra vez.


### Recomendaciones para aprender más

* [Velocidad del sonido (Hyperphysics)](http://hyperphysics.phy-astr.gsu.edu/hbasees/hframe.html)

* [Sonido (Ondas) (No me salen de Ricardo Cabrera)](https://ricuti.com.ar/no_me_salen/ondas/Ap_ond_sonido.html)

Author: Solmaz Khodaeifaal
