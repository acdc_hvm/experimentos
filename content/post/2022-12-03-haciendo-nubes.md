---
title: "Haciendo nubes"
author: Juan Pablo Carbajal, Maximiliano Baldiviezo, Agustín Valle, Mauricio Romano
subtitle: "☁️🚲🔩" # tres emojis que tengan que ver con el post, podés sacarlos de https://emojipedia.org/
tags: ["Nube", "Enfriamiento Adiabático", "Temperatura", "Presión", "Física"] # Por lo general contienen lo mismo que los temas
date: 2022-12-03 # poner la fecha de creación del post
math: false # si contiene ecuaciones poner true
image: false # a definir en el futuro que tipo de imágenes poner, por el momento queda false
--- 

En esta experiencia, proponemos simular el fenómeno atmosférico de una nube en el interior de una botella.

<!--more-->

---
* **Dificultad:** ★★☆☆☆

* **Materiales:** 
    * Botella con tapa de plástico a rosca
    * Válvula de bicicleta
    * Inflador
    * Cuchilla
    * Agua o alcohol

* **Duración de preparación:** 1 h

* **Temas:** Nube, Temperatura, Presión, Enfriamiento Adiabático, Física <!-- Los temas separados por comas. Por ej:  -->
* **Peligro:** ★★★☆☆ - *El mismo peligro que se corre al descorchar una botella*
---

### ¿Qué es y como se forma una nube?

Una nube es un conjunto de finas partículas de agua en estado líquido o en estado sólido (cristales de hielo) que forman masas de espesor, color y formas variables. La nube se forma cuando el vapor de agua que hay en el aire se condensa formando gotas pequeñas de agua líquida (o se sublima generando cristales de hielo). Así, propiciando una alta humedad en el aire y enfriándolo lo suficiente, podemos generar una nube.

En esta experiencia, proponemos simular el fenomeno atmosférico de una nube pero en el interior de una botella. Para ello, trabajaremos para aumentar la presión dentro de una botella (con las paredes humedecidas). Luego, abriremos la botella bruscamente, produciendo una disminución de la presión que solo será posible si también disminuye la temperatura, creando así las condiciones para que se formen las gotas de agua. Este proceso se conoce como enfriamiento adiabático. 

> Nota: El aire carece de tiempo para intercambiar calor con el ambiente y al expandirse hace trabajo, de manera que sólo es posible que haya una disminución de su energía interna y, por lo tanto, de su temperatura.

### ¿Cómo aumentar la presión? y ¿ donde buscar los elementos necesarios?
La principal característica de la botella es que tenga una **tapa de plástico a rosca**, pues *le haremos modificaciones*. En la foto te mostramos una botella de *500ml* que es muy útil para este tipo de experiencia. Las modificaciones que le haremos a la tapa permitirán que ingresemos aire a través de una válvula y, a su vez, que podamos provocar una salida de forma brusca.

![](/img/2022-12-03-haciendo-nubes/Botella.jpeg)

Lo que determinará el **tipo de válvula** que usarás es el *tipo de inflador* del que dispones. Si dispones de una bicicleta, te recomendamos sacarle una foto a la válvula que tiene  y pedir en una gomería que te regalen algunas en deshuso o comprar una en algún lugar donde vendan insumos para bicicleta. Sin embargo, es importante que independientemente del tipo, la válvula sea de **ajuste mediante tuerca** para optimizar el sellado. En Argentina, las válvulas más usuales son de tipo *Schrader*, que están presentes los neumáticos de los autos y se encuentran principalmente en los modelos de bicicletas más económicos. En las siguiente imagen te mostramos una válvula de este tipo. Se ve, también, que cuenta con una *arandela exterior* que ayuda al sellado.

![](/img/2022-12-03-haciendo-nubes/Valvula.jpeg)

#### Orificio para la válvula

Haremos un *orificio en la tapa*, que servirá para ingresar válvula, con algún elemento punzante (en la imagen se ve como usamos un destornillador). Luego sacamos la rebaba que queda con una cuchilla y, si el orificio sigue siendo menor que el diámetro de la válvula, lo agrandamos con la misma cuchilla hasta que tenga el tamaño suficiente. Incluso es mejor que sea un poco menor que el diámetro de la válvula, para que entre a presión y no le dejemos todo el trabajo a la tuerca de la válvula.

![](/img/2022-12-03-haciendo-nubes/Orificio.jpeg)

#### Recorte de la rosca

El trayecto que tiene la **rosca de la tapa** no nos permitiría un *cambio suficientemente brusco de la presión*. Para lograrlo, recortaremos la rosca dejando solo una hilera de topes (marcados en rojo en la figura). El método más seguro es cortarla en espiral con la cuchilla, de una forma similar a aquella mediante la cual se le extrae la cáscara a una fruta.

![](/img/2022-12-03-haciendo-nubes/Corte-rosca.jpeg)

#### Armado

¡Ahora sí! Tenemos la tapa toda bonita con sus modificaciones funcionales. Lo que sigue es ingresar la válvula (recordá que la arandela debe quedar por fuera de la botella) y sellamos con la rosca.

![](/img/2022-12-03-haciendo-nubes/Tapa-cortada-valvula.jpeg)

### ¡Manos a la obra!

Aumentaremos el nivel de humedad *mojando las paredes de la botella* con agua. Una opción que mejora el efecto de la nube es el uso de alcohol, podés hacer la comparación.

El próximo paso es sellar la botella con nuestra tapita valvular y comenzar a inflar. En la imagen se ve como usamos un inflador de mano, pero eso depende de lo que tengamos disponible. Podés aumentar la presión hasta que sientas, al tacto, algo similar a una botella de algún líquido con gas que está sin abrir.

![](/img/2022-12-03-haciendo-nubes/Inflando.jpeg)

***¡ATENCIÓN!*** a medida que aumente la presión debemos tener mucho cuidado en la *extracción del inflador* y en el *manejo de la botella* luego de ese momento. A partir de aquí tenés que imaginar que tenés en la mano una botella a punto de descorcharse. Pero si haces lo que te detallamos a continuación no tendrás de que preocuparte.

#### Extracción del inflador y preparación la apertura brusca

La siguiente imagen te muestra como podemos tomar la botella de manera que la tapa quede sujeta con nuestros de dedos índice y medio, lo que nos permite extraer el inflador asegurandonos de que la tapa no saldra. Luego, deslizas los mismos dedos según la secuencia mostrada que termina en un cierre del puño. Este cierre de puño nos da seguridad y será de gran utilidad para lograr abrir la botella rápidamente.

![](/img/2022-12-03-haciendo-nubes/preparando-descorche.jpeg)

#### ¡Descorche!

En los siguientes videos te mostramos cómo hacer el descorche. Se trata de la misma grabación en diferentes velocidades. Es importante que notes el movimiento de rotación contrario que generamos en la botella y la tapa, para aumentar al máximo el tiempo de apertura.


---

{{< youtube Kvl8jnSF3QU >}}

---

{{< youtube lmLj1aGgJyw >}}

---

Hermosa nube ¿verdad? Y además descorchamos para festejar! En medio del festejo, **¿te díste cuenta qué paso con la temperatura?**
