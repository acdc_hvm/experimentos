---
title: Preguntas frecuentes sobre Phyphox
subtitle: false
comments: false
date: 2022-03-22
lastmod: 2022-03-22
socialShare: false
---

Algunas traducidas del [Sitio de Phyphox](https://phyphox.org/faq/)

### ¿Tu app no está en español? 
Podes cambiar el idioma apretando el ícono ⓘ en la esquina superior derecha de tu pantalla.

### ¿El sensor que elegiste está bloqueado?
Es posible que tu teléfono no tenga este sensor pero ¡podes probar con otro teléfono!
