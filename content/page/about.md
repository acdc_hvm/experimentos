---
title: Sobre nosotros
subtitle: false
comments: false
date: 2022-03-22
lastmod: 2022-11-21
socialShare: false
---

En esta página se alojaran distintos experimentos diseñados y/o traducidos principalmente por miembros de la 
Asociación [Civil Ciencia al Alcance de Todos ACDC](http://www.acdc.org.ar/)
un grupo auto-convocado de ex-alumnos del Taller Física al Alcance de Todos y amigos de Daniel Córdoba (DC). 

También se aceptan contribuciones de cualquiera interesado en aportar un experimento, ver mas en la sección Contribuir.

Los contribuyentes son: 

* **Martín Aramayo:** Soy un físico, ex-alumno de Daniel Cordoba. Estudié en el colegio Belgrano N° 5095 la ex-Normal, la UNSa y el Instituto Balseiro.
    - 📧 [E-mail](mailto:blog.bar.trajeao@gmail.com)
    - 🌐 [Webpage](https://martinaramayo.gitlab.io/links) (Contiene mi blog y mis links.)
    - 📍 Buenos Aires, Argentina

* **Alejandra Mendez:** 
Soy física y trabajo en investigación (física atómica y molecular). Estudié en el IEM, la UNSa y la UBA. 
    - 📧 [E-mail](mailto:alemdz.7@gmail.com)
    - 📱 [Telegram](https://t.me/alejandr4mendez)
    - 🌐 [Webpage](https://sites.google.com/view/alemendez/)
    - 📍 Salta, Argentina

* **Juan Pablo Carbajal:** 
Soy físico con un doctorado en inteligencia artificial. 
Trabajo en investigación aplicada en la universidad tecnológica del este Suizo [OST](https://www.ost.ch). Estudié en el IEM, la UNLP, el Instituto Balsiero, la UBA, y la Universidad de Zürich.
    - 📧 [E-mail](mailto:ajuanpi@gmail.com)
    - 🌐 [Webpage](https://sites.google.com/site/juanpicarbajal)
    - 📍 Bad Ragaz, Suiza
